IP2Nation

Project home page: http://rachmat.net/en/2008/03/07/ip2nation-module-drupal

This IP2Nation module provides API to access ip2nation (http://www.ip2nation.com/) database in Drupal. With this module you can easily display visitor's country flag and name. This module originally developed
by Leontius Adhika Pradhana.


Requirement

Any version of drupal 5.


Installation

This module include ip2nation database but does not modify or add database schema in any way.


Contributors:
- Suryanto Rachmat
- Leontius Adhika Pradhana
